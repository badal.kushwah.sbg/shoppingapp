﻿using UserAPI.Model;

namespace UserAPI.Service
{
    public interface IUserService
    {
        List<User> GetAllUsers();
        bool DeleteUser(int id);
        User GetUserById(int id);
        //bool BlockUnBlockUser(int id, bool blockUnblockUser);
        User LogIn(UserLogin userLogin);
        bool RegisterUser(User user);
        bool EditUser(int id, User user);
        User GetUserByEmail(string email);
    }
}
