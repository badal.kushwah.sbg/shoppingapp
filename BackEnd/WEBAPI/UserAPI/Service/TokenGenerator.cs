﻿using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace UserAPI.Service
{
    public class TokenGenerator : ITokenGenerator
    {
        public string GenerateToken(int id, string email)
        {
            var userClaims = new Claim[]
             {
                new Claim(JwtRegisteredClaimNames.Jti,new Guid().ToString()),
                new Claim(JwtRegisteredClaimNames.UniqueName,email)

             };
            var userSecurityKey = Encoding.UTF8.GetBytes("jhytgfredswqazxcvbnmklpoiuytrewq");
            var userSymmetricSecurity = new SymmetricSecurityKey(userSecurityKey);
            var userSigninCredentials = new SigningCredentials(userSymmetricSecurity, SecurityAlgorithms.HmacSha256);
            var userJwtSecurityToken = new JwtSecurityToken
                (
                    issuer: "ProductApp",
                    audience: "APIProduct",
                    claims: userClaims,
                    expires: DateTime.UtcNow.AddMinutes(10),
                    signingCredentials: userSigninCredentials
                );
            var userSecurityTokenHandler = new JwtSecurityTokenHandler().WriteToken(userJwtSecurityToken);
            string jwtUserSecurityTokenHandler=JsonConvert.SerializeObject(new { Token = userSecurityTokenHandler });
            return jwtUserSecurityTokenHandler;
        }
    }
}
