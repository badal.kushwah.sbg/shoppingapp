﻿using UserAPI.Exception;
using UserAPI.Model;
using UserAPI.Repository;

namespace UserAPI.Service
{
    public class UserService : IUserService
    {
        readonly IUserRepository _userRepository;
        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        /*public bool BlockUnBlockUser(int id, bool blockUnblockUser)
        {
            User userExist = _userRepository.GetUserById(id);
            if(userExist == null)
            {
                return false;
            }
            else
            {
                int blockStatus =  _userRepository.BlockUnBlockUser(blockUnblockUser,userExist);
                return blockStatus==1?true:false;
            }
        }*/

        public bool DeleteUser(int id)
        {
            User userExist = _userRepository.GetUserById(id);
            if(userExist != null)
            {
                int userDeleteStatus = _userRepository.DeleteUser(userExist);
                return userDeleteStatus==1?true:false;
            }
            else
            {
                return false;
            }
        }
        public bool EditUser(int id, User user)
        {
            user.Id = id;
            /* User userExist = _userRepository.GetUserById(id);
             if (userExist == null)
             {
                 return false;
             }
             else
             {*/

                int editUserStatus = _userRepository.EditUser(user);
                return editUserStatus==1?true:false;
           // }
        }

        public List<User> GetAllUsers()
        {
            return _userRepository.GetAllUsers();
        }

        public User GetUserByEmail(string email)
        {
            return _userRepository.GetUserByEmail(email);
        }

        public User GetUserById(int id)
        {
            return _userRepository.GetUserById(id);
        }

        public User LogIn(UserLogin userLogin)
        {
            User user = _userRepository.LogIn(userLogin.Email, userLogin.Password);
            if(user!=null)
            {
                return user;
            }
            else
            {
                throw new UserCredentialInvalidException($"{userLogin.Email} and other details are invalid");
            }
        }

        public bool RegisterUser(User user)
        {
            User userExist = _userRepository.GetUserByEmail(user.Email);
            if( userExist == null)
            {
                int registerUserStatus = _userRepository.RegisterUser(user);
                return registerUserStatus==1?true:false;
            }
            else
            {
                throw new UserAlreadyExistException($"{user.Email} User Already Exist !!");
            }
        }
    }
}
