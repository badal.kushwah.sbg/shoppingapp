﻿using UserAPI.Model;

namespace UserAPI.Repository
{
    public interface IUserRepository
    {
        //int BlockUnBlockUser(bool blockUnblockUser, User userExist);
        User GetUserById(int id);
        int DeleteUser(User userExist);
        List<User> GetAllUsers();
        User LogIn(string email, string password);
        User GetUserByEmail(string email);
        int RegisterUser(User user);
        int EditUser(User user);
    }
}
