﻿using Microsoft.EntityFrameworkCore;
using UserAPI.Context;
using UserAPI.Model;

namespace UserAPI.Repository
{
    public class UserRepository : IUserRepository
    {
        UserDbContext _userDbContext;
        public UserRepository(UserDbContext userDbContext)
        {
            _userDbContext = userDbContext;
        }
        /*public int BlockUnBlockUser(bool blockUnblockUser, User userExist)
        {
            userExist.IsBlocked = blockUnblockUser;
            _userDbContext.Entry(userExist).State=EntityState.Modified;
            return _userDbContext.SaveChanges();
        }*/

        public int DeleteUser(User userExist)
        {
           _userDbContext.Remove(userExist);
            return _userDbContext.SaveChanges();
        }

        public int EditUser(User user)
        {
            _userDbContext.Entry(user).State = EntityState.Modified;
            return _userDbContext.SaveChanges();

        }

        public List<User> GetAllUsers()
        {
            return _userDbContext.userList.ToList();
        }

        public User GetUserById(int id)
        {
            return _userDbContext.userList.Where(u => u.Id == id).FirstOrDefault();
        }

        public User GetUserByEmail(string email)
        {
            return _userDbContext.userList.Where(u => u.Email == email).FirstOrDefault();
        }

        public User LogIn(string email, string password)
        {
            return _userDbContext.userList.Where(u => u.Email == email&&u.Password==password).FirstOrDefault();
        }

        public int RegisterUser(User user)
        {
            _userDbContext.userList.Add(user);
            return _userDbContext.SaveChanges();
        }
    }
}
