﻿namespace UserAPI.Exception
{
    public class UserCredentialInvalidException : ApplicationException
    {
        public UserCredentialInvalidException()
        {

        }
        public UserCredentialInvalidException(string msg) : base(msg)
        {

        }
    }
}
