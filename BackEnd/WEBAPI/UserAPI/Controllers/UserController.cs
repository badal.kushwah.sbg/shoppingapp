﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UserAPI.ExceptionAttribute;
using UserAPI.Model;
using UserAPI.Service;

namespace UserAPI.Controllers
{
    [ExceptionHandler]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        readonly IUserService _userService;
        readonly ITokenGenerator _tokenGenerator;
        public UserController(IUserService userService, ITokenGenerator tokenGenerator)
        {
            _userService = userService;
            _tokenGenerator = tokenGenerator;
        }
        [Route("GetAllUsers")]
        [HttpGet]
        public ActionResult GetAllUsers()
        {
            List<User> users = _userService.GetAllUsers();
            return Ok(users);
        }
        [Route("RegisterUser")]
        [HttpPost]
        public ActionResult RegisterUser(User user)
        {
            bool registerUserStatus = _userService.RegisterUser(user);
            return Ok(registerUserStatus);
        }
        [Route("DeleteUser/{id:int}")]
        [HttpDelete]
        public ActionResult DeleteUser(int id)
        {
            bool deleteUserStatus = _userService.DeleteUser(id);
            return Ok(deleteUserStatus);
        }
        [Route("GetUserById/{id:int}")]
        [HttpGet]
        public ActionResult GetUserById(int id)
        {
            User user = _userService.GetUserById(id);
            return Ok(user);
        }
        [Route("EditUser/{id:int}")]
        [HttpPut]
        public ActionResult EditUser(int id,User user)
        {
            bool editUserStatus = _userService.EditUser(id,user);
            return Ok(editUserStatus);
        }
        /*[Route("BlockUnBlock")]
        [HttpPut]
        public ActionResult BlockUnBlockUser(int id,bool blockUnblockUser)
        {
            bool blockUnblockUserStatus = _userService.BlockUnBlockUser(id,blockUnblockUser);
            return Ok(blockUnblockUserStatus);
        }*/

        [Route("LogIn")]
        [HttpPost]
        public ActionResult LogIn(UserLogin userLogin)
        {
            User user = _userService.LogIn(userLogin);
            string userToken = _tokenGenerator.GenerateToken(user.Id, user.Email);
           // HttpContext.Session.SetString("USERTOKEN", userToken);
            return Ok(userToken);
        }
        [Route("GetProductByEmail")]
        [HttpGet]
        public ActionResult GetUserByEmail(string email)
        {
            User user = _userService.GetUserByEmail(email);
            return Ok(user);
        }
    }
}
