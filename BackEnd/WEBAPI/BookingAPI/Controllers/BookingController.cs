﻿using BookingAPI.Model;
using BookingAPI.Service;
using Microsoft.AspNetCore.Mvc;

namespace BookingAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookingController : ControllerBase
    {
        readonly IBookingService _bookingService;
        public BookingController(IBookingService bookingService)
        {
            _bookingService = bookingService;
        }
        [Route("AddToCart")]
        [HttpPost]
        public ActionResult AddToCart(Order order)
        {
            bool addToCartStatus = _bookingService.AddToCart(order);
            return Ok(addToCartStatus);
        }

        [Route("GetAllCart")]
        [HttpGet]
        public ActionResult GetAllCart(string userEmail)
        {
            List<Order> order = _bookingService.GetAllCart(userEmail);
            return Ok(order);
        }
        [Route("DeleteToCard/{productId}/{userEmail}")]
        [HttpDelete]
        public ActionResult DeleteToCart(int productId,string userEmail)
        {
            bool deleteStatus = _bookingService.DeleteToCart(productId, userEmail);
            return Ok(deleteStatus);
        }
    }
}
