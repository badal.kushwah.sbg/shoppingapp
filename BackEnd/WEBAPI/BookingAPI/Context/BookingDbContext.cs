﻿using BookingAPI.Model;
using Microsoft.EntityFrameworkCore;

namespace BookingAPI.Context
{
    public class BookingDbContext : DbContext
    {
        public BookingDbContext()
        {

        }
        public BookingDbContext(DbContextOptions<BookingDbContext> Context) : base(Context)
        {

        }

        public DbSet<Order> cartShopping { get; set; }
    }
}
