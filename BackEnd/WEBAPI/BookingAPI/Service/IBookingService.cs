﻿using BookingAPI.Model;

namespace BookingAPI.Service
{
    public interface IBookingService
    {
        bool AddToCart(Order order);
        List<Order> GetAllCart(string userEmail);
        bool DeleteToCart(int productId, string userEmail);
    }
}
