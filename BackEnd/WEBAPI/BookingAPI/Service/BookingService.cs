﻿using BookingAPI.Model;
using BookingAPI.Repository;

namespace BookingAPI.Service
{
    public class BookingService : IBookingService
    {
        readonly IBookingRepository _bookingRepository;
        public BookingService(IBookingRepository bookingRepository)
        {
            _bookingRepository = bookingRepository;
        }
        public bool AddToCart(Order order)
        {
            int addToCardStatus = _bookingRepository.AddToCart(order);
            return addToCardStatus ==1 ? true : false;
        }

        public bool DeleteToCart(int productId, string userEmail)
        {
            Order order = _bookingRepository.GetOrderByProductIdAndUserEmail(productId, userEmail);
            if(order != null)
            {
                int deleteStatus = _bookingRepository.DeleteTocard(order);
                return deleteStatus == 1 ? true : false;
            }
            else
            {
                return false;
            }
        }

        public List<Order> GetAllCart(string userEmail)
        {
            return _bookingRepository.GetAllCart(userEmail);
        }
    }
}
