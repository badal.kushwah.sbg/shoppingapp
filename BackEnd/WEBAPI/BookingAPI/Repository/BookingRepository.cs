﻿using BookingAPI.Context;
using BookingAPI.Model;

namespace BookingAPI.Repository
{
    public class BookingRepository : IBookingRepository
    {
        readonly BookingDbContext _bookingDbContext;
        public BookingRepository(BookingDbContext bookingDbContext)
        {
            _bookingDbContext = bookingDbContext;
        }

        public int AddToCart(Order order)
        {
           _bookingDbContext.cartShopping.Add(order);
            return _bookingDbContext.SaveChanges();
        }

        public int DeleteTocard(Order order)
        {
            _bookingDbContext.cartShopping.Remove(order);
            return _bookingDbContext.SaveChanges();
        }

        public List<Order> GetAllCart(string userEmail)
        {
            return _bookingDbContext.cartShopping.Where(o => o.UserEmail.Equals(userEmail)).ToList();
        
        }

        public Order GetOrderByProductIdAndUserEmail(int productId, string userEmail)
        {
            return _bookingDbContext.cartShopping.Where(p => p.ProductId == productId && p.UserEmail.Equals(userEmail)).FirstOrDefault();
        }
    }
}
