﻿using BookingAPI.Model;

namespace BookingAPI.Repository
{
    public interface IBookingRepository
    {
        int AddToCart(Order order);
        List<Order> GetAllCart(string userEmail);
        int DeleteTocard(Order order);
        Order GetOrderByProductIdAndUserEmail(int productId, string userEmail);
    }
}
