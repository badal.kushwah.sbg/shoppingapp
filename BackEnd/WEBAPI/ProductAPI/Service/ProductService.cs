﻿using ProductAPI.Model;
using ProductAPI.Repository;

namespace ProductAPI.Service
{
    public class ProductService : IProductService
    {
        readonly IProductRepository _productRepository;
        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }
        public bool AddProduct(Product product)
        {
            Product productExist = _productRepository.GetProductByName(product.Name);
            if (productExist == null)
            {
                int addProductStatus = _productRepository.AddProduct(product);
                return addProductStatus == 1 ? true : false;
            }
            else
            {
                return false;
            }
        }
        public bool DeleteProduct(int id)
        {
            Product productExist = _productRepository.GetProductById(id);
            if (productExist != null)
            {
                int productDeleteStatus = _productRepository.DeleteProduct(productExist);
                return productDeleteStatus == 1 ? true : false;
            }
            else
            {
                return false;
            }
        }
        public bool EditProduct(int id, Product product)
        {
            product.Id = id;
            /* Product productExist = _productRepository.GetProductById(id);
             if (productExist == null)
             {
                 return false;
             }
             else
             {*/

            int editProductStatus = _productRepository.EditProduct(product);
            return editProductStatus == 1 ? true : false;
            // }
        }

        public List<Product> GetAllProducts()
        {
            return _productRepository.GetAllProducts();
        }
        public Product GetProductById(int id)
        {
            return _productRepository.GetProductById(id);
        }
    }
}
