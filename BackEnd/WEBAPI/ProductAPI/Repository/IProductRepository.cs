﻿using ProductAPI.Model;

namespace ProductAPI.Repository
{
    public interface IProductRepository
    {
        Product GetProductByName(string? name);
        int AddProduct(Product product);
        Product GetProductById(int id);
        int DeleteProduct(Product productExist);
        int EditProduct(Product product);
        List<Product> GetAllProducts();
    }
}
