﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProductAPI.Model;
using ProductAPI.Service;

namespace ProductAPI.Controllers
{
   // [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        readonly IProductService _productService;
        public ProductController(IProductService productService)
        {
            _productService = productService;
        }
        [Route("AddProduct")]
        [HttpPost]
        public ActionResult AddProduct(Product product)
        {
            bool addProductStatus = _productService.AddProduct(product);
            return Ok(addProductStatus);
        }
        [Route("GetAllProducts")]
        [HttpGet]
        public ActionResult GetAllProducts()
        {
            List<Product> products = _productService.GetAllProducts();
            return Ok(products);
        }
        
        [Route("DeleteProduct/{id:int}")]
        [HttpDelete]
        public ActionResult DeleteProduct(int id)
        {
            bool deleteProductStatus = _productService.DeleteProduct(id);
            return Ok(deleteProductStatus);
        }
        [Route("GetProductById/{id:int}")]
        [HttpGet]
        public ActionResult GetProductById(int id)
        {
            Product product = _productService.GetProductById(id);
            return Ok(product);
        }
        [Route("EditProduct/{id:int}")]
        [HttpPut]
        public ActionResult EditProduct(int id,Product product)
        {
            bool editProductStatus = _productService.EditProduct(id, product);
            return Ok(editProductStatus);
        }
        
       
    }
}
