export class Order {
    id?:number
    productId?:number
    productName?:string
    productDescription?:string
    productPrice?:number
    productImg?:string
    userEmail?:string
}
