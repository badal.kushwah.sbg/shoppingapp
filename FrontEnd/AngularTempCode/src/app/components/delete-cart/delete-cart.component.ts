import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/service/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-delete-cart',
  templateUrl: './delete-cart.component.html',
  styleUrls: ['./delete-cart.component.css']
})
export class DeleteCartComponent implements OnInit {
  productId=0
  userEmail=""

  constructor(private route:ActivatedRoute,private router:Router,private userService:UserService) {
    this.route.paramMap.subscribe( params => {
      this.userEmail=String(params.get('email'))
      this.productId=Number(params.get('id'))
  
    });
   }

  ngOnInit(): void {
    this.DeleteCart(this.userEmail,this.productId)
  }
  DeleteCart(userEmail: string, productId: number) 
  {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => 
    {

      if (result.isConfirmed) 
      {
          this.userService.DeleteCart(userEmail,productId).subscribe(res=>
          {
              if(res)
              {
                console.log("True") 
                Swal.fire(
                  'Deleted!',
                  'Your Product has been deleted.',
                  'success'
                )
                this.router.navigateByUrl(`/cartDashboard/${this.userEmail}`)
                //this.router.navigate([`/dashboard`,this.userEmail]);
              }
              else
              { 
                console.log("False")
                Swal.fire
                ({
                  icon: 'error',
                  title: 'Oops...',
                  text: 'Something went wrong!',
                })
              } 
          })
      }
      else
      {
        this.router.navigateByUrl(`/cartDashboard/${this.userEmail}`);
      }
    })   

  }

}
