import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductService } from 'src/app/service/product.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-product-delete',
  templateUrl: './product-delete.component.html',
  styleUrls: ['./product-delete.component.css']
})
export class ProductDeleteComponent implements OnInit {
  productId=0;

  constructor(private productService:ProductService,private router:ActivatedRoute,private rout:Router) { }

  ngOnInit(): void {
    this.DeleteProduct()
  }
  DeleteProduct()
  {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) 
      {
        this.router.params.subscribe(data=>{
          this.productId=data['id']
          this.productService.DeleteProduct(this.productId).subscribe(res=>{
          if(res){
            console.log("True") 
            this.rout.navigateByUrl('/adminDashboard')}
          else{console.log("False")} 
          })
        })
        
        Swal.fire(
          'Deleted!',
          'Your Product has been deleted.',
          'success'
        )
      }
      else
      {
        this.rout.navigateByUrl('/adminDashboard');
      }
    })    
  }

}


