import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';

import { UserService } from 'src/app/service/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
//user:User 
  constructor(private userService:UserService,private router:Router) { 
  //  this.user=new User
    console.log("This is register components");
  }

  ngOnInit(): void {
    //this.RegisterUser();
  }
  RegisterUser(user:User) {
    // this.user.name="Patel"
    // this.user.password="lavish"
    // this.user.email="l@gmail.com"
    // this.user.location="Indore"
    console.log(user.name)
    console.log(user.email)
    console.log(user.location)
    console.log(user.password)
    this.userService.RegisterUser(user).subscribe({next:res=>
    {
      console.log(res)
      if(res)
      {
        console.log("True")
        Swal.fire
        (
          'User Registration',
          'Registration Success',
          'success'
        )
        this.router.navigateByUrl('/login');
    }
    else
    {
      Swal.fire
      ({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!',
      })
    }
     },

     error:(err)=>{
         console.error(err);
      
         Swal.fire({
      
         icon: 'error',
      
           title: 'error!',
      
           text: `${err.error.text}`
       })
      
       }

    })
  }

}


