import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from 'src/app/models/product';
import { ProductService } from 'src/app/service/product.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.css']
})
export class ProductAddComponent implements OnInit {

  constructor(private productService:ProductService,private router:Router) { }

  ngOnInit(): void {
  }

  AddProduct(product:Product) 
  {
    
    this.productService.AddProduct(product).subscribe(res=>
    {
      console.log(res)
      if(res)
      {
        console.log("True")
        Swal.fire
        (
          'Add Product',
          'Product Add Successfully',
          'success'
        )
        this.router.navigateByUrl('/adminDashboard');
    }
    else
    {
      Swal.fire
      ({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!',
      })
    }
     })
  }

}
