import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from 'src/app/models/product';
import { UserLogin } from 'src/app/models/user-login';

import { ProductService } from 'src/app/service/product.service';
import { UserService } from 'src/app/service/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  products?:Product[];
  userEmail?:string 
   
  constructor(private productService:ProductService,private userService:UserService,private router:ActivatedRoute,private route:Router) { 
    console.log("This is Dashboard Constructor");
  }

  ngOnInit(): void {
    console.log("This is Method Afer OnInt");
    this.getAllProduct()
    this.router.params.forEach(data=>{
      this.userEmail=data['email'] })
      console.log(this.userEmail)
    //this.getAllUser()    
  }
  getAllUser() {
    this.userService.getAllUser().subscribe(res=>{
      console.log(res)
     
    })
  }
  getAllProduct() {
    this.productService.getAllProduct().subscribe(res=>{
      console.log(res)
      this.products=res;
    })
  }
  Order()
  {
    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Page Not Found',
    })
    this.route.navigate([`/dashboard`,this.userEmail]);
  }

}
