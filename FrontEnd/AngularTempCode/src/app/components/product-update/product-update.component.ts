import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { ProductService } from 'src/app/service/product.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-product-update',
  templateUrl: './product-update.component.html',
  styleUrls: ['./product-update.component.css']
})
export class ProductUpdateComponent implements OnInit {
   edit = new FormGroup({
     name: new FormControl(),
     description: new FormControl(),
     price: new FormControl(),
     imgPath:new FormControl()
   })
  
  constructor(private productService:ProductService,private router:ActivatedRoute,private rout:Router) {
   
   }

  ngOnInit(): void {
    this.GetProductById()
    //this.DeleteProduct()
  }
  GetProductById()
  {
    this.productService.GetProductById(this.router.snapshot.params['id']).subscribe(res=>{
      console.log(res)
      this.edit=new FormGroup({
        name: new FormControl(res['name']),
        description: new FormControl(res['description']),
        price: new FormControl(res['price']),
        imgPath: new FormControl(res['imgPath'])
         })
    })
  }
  UpdateProduct()
  {
     console.log("UpdateProduct")
      this.productService.UpdateProduct(this.router.snapshot.params['id'],this.edit.value).subscribe(res=>{
        if(res)
      {
        console.log("True")
        Swal.fire
        (
          'Product Update',
          'Product Update Sucessfully',
          'success'
        )
        this.rout.navigateByUrl('/adminDashboard');
    }
    else
    {
      Swal.fire
      ({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!',
      })
    }
      })
  }
  
  

}
