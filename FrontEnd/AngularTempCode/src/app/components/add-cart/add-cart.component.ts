import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Order } from 'src/app/models/order';
import { User } from 'src/app/models/user';
import { ProductService } from 'src/app/service/product.service';
import { UserService } from 'src/app/service/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-cart',
  templateUrl: './add-cart.component.html',
  styleUrls: ['./add-cart.component.css']
})
export class AddCartComponent implements OnInit {
  userEmail:string | null | undefined
  productId=0
  userId=0
  order: Order

 
  constructor(private productService:ProductService,private route: ActivatedRoute,private userService:UserService,private router:Router) {
    this.order= new Order;
    this.route.paramMap.subscribe( params => {
      this.userEmail=params.get('email')
      this.productId=Number(params.get('id'))
  
    });
  }
  ngOnInit(): void {
    this.addCart()
    console.log(this.productId)
  }

  addCart()
  {
    this.productService.GetProductById(this.productId).subscribe((res)=>{
       
      this.order.productId=this.productId
      this.order.productName=res['name']
      this.order.productDescription=res['description']
      this.order.productPrice=res['price']
      this.order.productImg=res['imgPath']
      this.order.userEmail=String(this.userEmail)
       this.userService.AddCart(this.order).subscribe(res1=>{
       if(res1)
       {
        Swal.fire
        (
          `Cart Added`,
          'Cart Added Successfully',
          'success'
        )
        this.router.navigate([`/dashboard`,this.userEmail]);
       }
       else
       {
        Swal.fire
        ({
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong!',
        })
       }
     })
    })
  }

}
