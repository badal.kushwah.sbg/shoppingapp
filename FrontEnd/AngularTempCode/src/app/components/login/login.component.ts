import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserLogin } from 'src/app/models/user-login';
import { UserService } from 'src/app/service/user.service';
import Swal from 'sweetalert2';
import { DashboardComponent } from '../dashboard/dashboard.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  constructor(private userService:UserService,private router:Router) {
    console.log("This is register components");
    
   }

  ngOnInit(): void {
  }
  Login(userLogin:UserLogin) {
    console.log(userLogin)
    if(userLogin.email=="Admin@gmail.com"&&userLogin.password=="Admin@123")
    {
      Swal.fire
      (
        'You Are Admin',
        'Admin Login Success',
        'success'
      )
      this.router.navigateByUrl('/adminDashboard');
    }
    else
    {
          this.userService.Login(userLogin).subscribe({next:(res)=>{
            let jsonObject = JSON.stringify(res)
            let jsonToken = JSON.parse(jsonObject)
            console.log(`User Token After Login::${jsonToken['Token']}`)
            localStorage.setItem('userToken',jsonToken['Token'])
            Swal.fire
            (
              `User Login`,
              'User Login Success',
              'success'
            )
            this.router.navigate([`/dashboard`,userLogin.email]);
          },
          error:(err)=>{
            console.error(err);

            Swal.fire({

              icon: 'error',

              title: 'error!',

              text: `${err.error.text}`
          })
      
        }
      })
    }
  }

}
