import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models/product';
import { ProductService } from 'src/app/service/product.service';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {
  products?:Product[];
  constructor(private productService:ProductService) { console.log("This is admin dashboard") }

  ngOnInit(): void {
    this.getAllProduct()

  }
  getAllProduct() {
    this.productService.getAllProduct().subscribe(res=>{
      console.log(res)
      this.products=res;
    }
    )
  }
 


}
