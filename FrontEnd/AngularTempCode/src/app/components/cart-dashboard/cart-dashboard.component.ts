import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Order } from 'src/app/models/order';
import { UserService } from 'src/app/service/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-cart-dashboard',
  templateUrl: './cart-dashboard.component.html',
  styleUrls: ['./cart-dashboard.component.css']
})
export class CartDashboardComponent implements OnInit {
  userEmail=""
  orders?:Order[];
  constructor(private route:ActivatedRoute,private userService:UserService,private router : Router) {
    
    this.route.paramMap.subscribe( params => {
      this.userEmail=String(params.get('email'))
      console.log(`${this.userEmail} this is cart dashboard`)
    })
  }

  ngOnInit(): void {
    this.GetAllCart()
  }
  GetAllCart()
  {
      this.userService.GetAllCart(this.userEmail).subscribe(res=>{
        console.log(res);
        this.orders=res
      })
  }
  Order()
  {
    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Page Not Found',
    })
    this.router.navigate([`/cartDashboard`,this.userEmail]);
  }

}
