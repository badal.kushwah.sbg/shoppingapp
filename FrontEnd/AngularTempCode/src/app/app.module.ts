import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HttpClientModule } from '@angular/common/http';
import { HeaderComponent } from './components/header/header.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import { AdminLoginComponent } from './components/admin-login/admin-login.component';
import { AdminHeaderComponent } from './components/admin-header/admin-header.component';
import { AdminDashboardComponent } from './components/admin-dashboard/admin-dashboard.component';
import { ProductUpdateComponent } from './components/product-update/product-update.component';
import { ProductDeleteComponent } from './components/product-delete/product-delete.component';
import { ProductAddComponent } from './components/product-add/product-add.component';
import { LogoutComponent } from './components/logout/logout.component';
import { AddCartComponent } from './components/add-cart/add-cart.component';
import { CartDashboardComponent } from './components/cart-dashboard/cart-dashboard.component';
import { DeleteCartComponent } from './components/delete-cart/delete-cart.component'



@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    DashboardComponent,
    HeaderComponent,
    AdminLoginComponent,
    AdminHeaderComponent,
    AdminDashboardComponent,
    ProductUpdateComponent,
    ProductDeleteComponent,
    ProductAddComponent,
    LogoutComponent,
    AddCartComponent,
    CartDashboardComponent,
    DeleteCartComponent,
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    MatInputModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
