import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth.guard';
import { AddCartComponent } from './components/add-cart/add-cart.component';
import { AdminDashboardComponent } from './components/admin-dashboard/admin-dashboard.component';
import { AdminLoginComponent } from './components/admin-login/admin-login.component';
import { CartDashboardComponent } from './components/cart-dashboard/cart-dashboard.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { DeleteCartComponent } from './components/delete-cart/delete-cart.component';
import { HeaderComponent } from './components/header/header.component';
import { LoginComponent } from './components/login/login.component';
import { LogoutComponent } from './components/logout/logout.component';
import { ProductAddComponent } from './components/product-add/product-add.component';
import { ProductDeleteComponent } from './components/product-delete/product-delete.component';
import { ProductUpdateComponent } from './components/product-update/product-update.component';
import { RegisterComponent } from './components/register/register.component';

const routes: Routes = [
  {path:'register',component:RegisterComponent},
  {path:'login',component:LoginComponent},
  {path:'dashboard/:email',component:DashboardComponent,canActivate:[AuthGuard]},
  {path:'adminLogin',component:AdminLoginComponent},
  {path:'adminDashboard',component:AdminDashboardComponent},
  {path:'productUpdate/:id',component:ProductUpdateComponent},
  {path:'productDelete/:id',component:ProductDeleteComponent},
  {path:'productAdd',component:ProductAddComponent},
  {path:'logout',component:LogoutComponent},
  {path:'',component:HeaderComponent,pathMatch:"full"},
  {path:'addCart/:email/:id',component:AddCartComponent},
  {path:'cartDashboard/:email',component:CartDashboardComponent},
  {path:'deleteCart/:email/:id',component:DeleteCartComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
