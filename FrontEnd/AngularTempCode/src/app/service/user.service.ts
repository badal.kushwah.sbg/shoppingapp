import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Order } from '../models/order';
import { User } from '../models/user';
import { UserLogin } from '../models/user-login';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  
  
  
  
  
  
  constructor(private httpClient:HttpClient) { }
  RegisterUser(user:User):Observable<boolean>{
    return this.httpClient.post<boolean>('https://localhost:44308/api/User/RegisterUser',user)
  }
  Login(userLogin: UserLogin):Observable<string> {
    return this.httpClient.post<string>('https://localhost:44308/api/User/LogIn',userLogin)
  }
  getAllUser():Observable<User[]> {
    return this.httpClient.get<User[]>('https://localhost:44335/api/Product/GetAllProducts')
  }
  GetUserByEmail(userEmail: string| null | undefined) {
    return this.httpClient.get<User>(`https://localhost:44308/api/User/GetProductByEmail?email=${userEmail}`)
  }
    //Cart realated Method
  AddCart(order:Order) {
      return this.httpClient.post<boolean>('https://localhost:7256/api/Booking/AddToCart',order)
  }
  GetAllCart(userEmail: string) {
      return this.httpClient.get<Order[]>(`https://localhost:7256/api/Booking/GetAllCart?userEmail=${userEmail}`)
  }
  DeleteCart(userEmail: string, productId: number) {
    return this.httpClient.delete<boolean>(`https://localhost:7256/api/Booking/DeleteToCard/${productId}/${userEmail}`)
  }
  
  
}
