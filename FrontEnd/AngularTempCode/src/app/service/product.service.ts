import { Injectable } from '@angular/core';
import{HttpClient} from '@angular/common/http'
import { Observable } from 'rxjs';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
 
  constructor(private httpClient:HttpClient) { }
  getAllProduct():Observable<Product[]> {
    return this.httpClient.get<Product[]>("https://localhost:44335/api/Product/GetAllProducts")
  }
  GetProductById(id: number) {
    return this.httpClient.get<Product>(`https://localhost:44335/api/Product/GetProductById/${id}`)
  }
  UpdateProduct(id:number,product:Product){
     return this.httpClient.put(`https://localhost:44335/api/Product/EditProduct/${id}`,product)
  }
  DeleteProduct(productId: number) {  
    return this.httpClient.delete(`https://localhost:44335/api/Product/DeleteProduct/${productId}`)   
  }
  AddProduct(product: Product) {
    return this.httpClient.post('https://localhost:44335/api/Product/AddProduct',product)
  }
 
}
